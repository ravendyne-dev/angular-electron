import { app, BrowserWindow } from "electron";
import path from "path";


function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    icon: path.join(__dirname, 'ui/favicon.png'),
    width: 1024,
    height: 768,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
      // Renderer process can't call Node side and we use context isolation.
      // We instead use https://www.electronjs.org/docs/api/context-bridge to expose Node-based API
      // which can then be called in src/services/remote service implementations
      // through globals defined via contextBridge
      nodeIntegration: false,
      contextIsolation: true,
    },
  });

  // Load the dist folder from Angular.
  mainWindow.loadFile(path.join(__dirname, "ui/index.html"));

  // Open the DevTools.
  if( ! app.isPackaged ) {
    mainWindow.webContents.openDevTools();
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
  createWindow();

  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
