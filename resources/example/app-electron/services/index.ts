import { ExampleServiceMain } from "./example.service";

const apiServices = {
  MyExampleServiceMain: null,
};

export const setupAPIServicesMain = () => {
  apiServices.MyExampleServiceMain = new ExampleServiceMain();
}
