// Node integration is disabled and context isolation is enabled.
// Here we just export API exposed via contextBridge on ipcMain side of the things.

import { from, Observable } from "rxjs";
import { APIBridgeRendererItnf } from "src/app-electron/services/api-bridge.renderer.interface";


// We define 'apiBridge' global using contextBridge.exposeInMainWorld()
const apiBridge = (window as any).apiBridge as APIBridgeRendererItnf;

export class APIBridge {
  public static fetch( method: string, ...args: any[] ) : Observable<any> {
    return from( apiBridge.fetch( method, ...args ) );
  }
}
