```ts
import url from "url";

  // https://github.com/electron/electron/issues/14978#issuecomment-535191746
  // handle issue with Angular Router ctrl+R
  // TODO how does this work with routes?
  mainWindow.webContents.on('did-fail-load', () => {
    console.log('did-fail-load');
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'ui/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  });
```
