import { Component } from '@angular/core';
import { ExampleService } from './services/example.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-electron';

  constructor(
    private exampleService: ExampleService,
  ) {}

  do42() {
    this.exampleService.addForyTwo( 7 ).subscribe( r => { console.log('42', r) });
  }

  do47() {
    this.exampleService.addForySeven( 7 ).subscribe( r => { console.log('47', r) });
  }
}
