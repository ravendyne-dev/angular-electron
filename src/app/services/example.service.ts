import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { APIBridge } from "./api-bridge";


@Injectable({
  providedIn: 'root'
})
export class ExampleService {

  constructor() {}

  private readonly exampleMethod = 'example/method';
  private readonly exampleMethodTwo = 'example/method/two';

  addForyTwo( value: number ) : Observable<number> {
    return APIBridge.fetch( this.exampleMethod, value );
  }

  addForySeven( value: number ) : Observable<number> {
    return APIBridge.fetch( this.exampleMethodTwo, value );
  }
}
