import { ipcMain } from 'electron';

export type MainAPIBridgeHandler = (...args: any[]) => Promise<any>;

export class APIBridgeMain {

  public static serve( method: string, handler: MainAPIBridgeHandler ) {

    const apiChannel = `api-bridge/${method}`;

    ipcMain.removeAllListeners( apiChannel );

    ipcMain.handle( apiChannel, async (event: Electron.IpcMainEvent, ...args: any[]) : Promise<any> => {
      return handler(...args).catch( reason => {
        console.error(`Error processing request for "${method}":`, reason);
      });
    });
  }

}
