import { APIBridgeMain } from './api-bridge.main';


export class ExampleServiceMain {

  private readonly exampleMethod = 'example/method';
  private readonly exampleMethodTwo = 'example/method/two';

  constructor() {
    APIBridgeMain.serve( this.exampleMethod, (...args: any[]) : Promise<any> => { return this.addFortyTwo( args[0] ) } );
    APIBridgeMain.serve( this.exampleMethodTwo, (...args: any[]) : Promise<any> => { return this.addFortySeven( args[0] ) } );
  }

  addFortyTwo( value: number ) : Promise<number> {
    return Promise.resolve( value + 42 );
  }

  addFortySeven( value: number ) : Promise<number> {
    return Promise.resolve( value + 47 );
  }
}
