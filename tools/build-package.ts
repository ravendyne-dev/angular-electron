// const packager = require('electron-packager')
import packager from 'electron-packager';


const output_folder = 'build';

const ignored = [
  // 'build',
  // 'dist',
  'docs',
  'e2e',
  // 'node_modules',
  'src',
  'tools',
  '.editorconfig',
  '.gitignore',
  'angular.json',
  'browserslist',
  'karma.conf.js',
  'package-lock.json',
  // 'package.json',
  'README.md',
  'tsconfig.app.json',
  'tsconfig.el.json',
  'tsconfig.elp.json',
  'tsconfig.json',
  'tsconfig.spec.json',
  'tslint.json',
  'webpack.config.elp.js',
];


(async () => {
  try {

    // 'linux' platform app icon is set via BrowserWindow.icon in app-electron/main.ts
    const appIcon = process.platform == 'darwin' ? 'src/osx_icon.icns' : process.platform == 'win32' ? 'src/favicon.ico' : undefined;

    const ignore_regex : RegExp[] = [];
    for( const i of ignored ) ignore_regex.push( new RegExp(i) )
    const options: packager.Options = {
      // name: '', // package.productName or package.name
      dir: '.', // source directory
      out: output_folder,
      overwrite: true, // re-create output folder for a given platform
      ignore: ignore_regex,

      // this will be set to a value for 'win32' and 'darwin', for 'linux' it will be undefined, a.k.a. not set
      icon: appIcon,

      // appVersion: '1.0.0', // package.version
      // arch: 'x64',
      // platform: 'linux',
      // executableName: 'my_app', // defaults to 'name' option
      // prune: true, // default
    }

    const appPaths = await packager(options)
    console.log(`Electron app bundles created:\n${appPaths.join("\n")}`)

  } catch (err) {
    console.log(err);
    process.exit(1);
  }
})();
